#untuk keperluan scrapping
from playwright.sync_api import sync_playwright
from dataclasses import dataclass, asdict, field

#untuk pengolahan data
import pandas as pd
import sys
import math

#untuk tanggal
import datetime

#untuk generate id review
import secrets
import random
import string


## dikarenakan tidak id dari review di trip advisor, perlu dibuat id secara manual setiap kali pengambilan review 
def generate_review_id():
    return secrets.token_urlsafe(26)

# jika ada error, data disimpan dalam excel sebelum program berhenti
def save_and_exit(review_list, filename):
    review_list.save_to_excel(filename)
    sys.exit("Script exited due to an error.")


@dataclass
class Review:
    place: str = "Pantai Sanur"
    id_review: str = generate_review_id() 
    collecting_time:str = None 
    review_time: str = None 
    username: str = None 
    rating: str = None
    review_text: str = None

@dataclass
class ReviewList:
    """
    kelas berikut untuk menampung semua review
    """

    review_list: list[Review] = field(default_factory=list)

    def dataframe(self):
        """transform review_list to pandas dataframe

        Returns: pandas dataframe
        """
        return pd.json_normalize((asdict(review) for review in self.review_list), sep="_")

    def save_to_excel(self, filename):
        """saves pandas dataframe to excel (xlsx) file

        Args:
            filename (str): filename
        """
        self.dataframe().to_excel(f"{filename}.xlsx", index=False)

def main():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        page = browser.new_page()
        page.goto("https://www.tripadvisor.co.id/Attraction_Review-g297700-d1721869-Reviews-Sanur_Beach-Sanur_Denpasar_Bali.html",timeout=60000)

        page.mouse.wheel(0, 1570)

        # inisiasi kelas
        review_list = ReviewList()

        n = math.ceil(total/10)
        
        now = datetime.datetime.now()
        formatted_now = now.strftime("%Y-%m-%d")

        print(" ~~~~~~~~~~ Scraping by 3SD1 ~~~~~~~~~~")
        print(f"Diambil pada : {formatted_now}") 

        for i in range(1,n+1):
            for j in range(1,11):

                page.wait_for_timeout(1500)

                new_review = Review()

                # collecting time
                current_datetime = now
                formatted_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")
                
                # reviewer name
                reviewerName = '//*[@id="tab-data-qa-reviews-0"]/div/div[5]/div/div[' + str(j) + ']/div/div/div[1]/div[1]/div[2]/span/a'
                try:
                    reviewer_name = page.locator(reviewerName).inner_text()
                except:                    
                    print(f"Error: Nama reviewer tidak ditemukan di halaman {i}, review {j}")                    

                # review time
                for div_index in [8, 7, 6, 5, 4, 3, 2, 1]:
                    xpath_reviewTime = f'//*[@id="tab-data-qa-reviews-0"]/div/div[5]/div/div[{j}]/div/div/div[{div_index}]/div[1]'
                    time_locator = page.locator(xpath_reviewTime)

                    # Cek apakah element review time sudah ada dan visible
                    if time_locator.is_visible():
                        review_time = time_locator.inner_text()
                        print(f"Review Time: {review_time}")
                        break  # Break jika sudah ketemu
                    else:
                        print(f"Review Time untuk div[{div_index}] tidak ditemukan atau tidak terlihat [non-visible].")

                # rating score
                rating = "//html[1]/body[1]/div[1]/main[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/section[7]/div[1]/div[1]/div[1]/section[1]/section[1]/div[1]/div[1]/div[5]/div[1]/div["+str(j)+"]/div[1]/div[1]/div[2]/*[name()='svg'][1]"
                try:
                    rating = page.query_selector(rating).get_attribute('aria-label')
                except:
                    print(f"Error: Rating skor xpath tidak ditemukan di halaman {i}, review {j}")

                review = '//*[@id="tab-data-qa-reviews-0"]/div/div[5]/div/div['+str(j)+']/div/div/div[5]'

                if "Selengkapnya" in page.locator(review).inner_text():
                    xpath_button = '//*[@id="tab-data-qa-reviews-0"]/div/div[5]/div/div['+str(j)+']/div/div/div[5]/div[2]/button/span'
                    page.locator(xpath_button).click();
            
                review_text = page.locator(review).inner_text()
                
                # memasukkan variabel ke class Review
                # review id dibuat random dikarenakan tidak ada id review pada website tripadvisor
                new_review.id_review = generate_review_id()
                new_review.collecting_time = formatted_datetime
                new_review.review_time = review_time
                new_review.rating = rating
                new_review.username = reviewer_name
                new_review.review_text = review_text

                review_list.review_list.append(new_review)
                page.wait_for_timeout(500)

                # Print halaman, nomor, dan empat huruf pertama dari nama reviewer
                print(f"Halaman : {i}  , No : {j}   | Reviewer Name: {reviewer_name[:4]}")

                # Klik tombol next jika sudah di akhir halaman dan semua review pada halaman itu sudah terscrape.
                if(j%10==0):
                    page.wait_for_timeout(1000)
                    next_xpath = '//*[@id="tab-data-qa-reviews-0"]/div/div[5]/div/div[11]/div[1]/div/div[1]/div[2]/div/a'
                    page.locator(next_xpath).click()
                    page.wait_for_timeout(1200)

                page.wait_for_timeout(900)
                #scroll
                page.mouse.wheel(0, 350)
            
        page.wait_for_timeout(5000)
        print("\n======== Menyimpan ke Excel ========")
        review_list.save_to_excel("pantai_sanur_review_tripadvisor")

        browser.close()

if __name__ == "__main__":

    """
    pada halaman tripadvisor, tiap halaman memuat 10 review sehingga untuk mempermudah proses scrapping, jumlah review harus kelipatan 10 
    """

    while True:
        try:
            jumlah = input("Jumlah review (dalam kelipatan 10) : ")
            total = int(jumlah)
            if total % 10 == 0:
                break
            else:
                print("Masukkan harus kelipatan 10. Silakan coba lagi.")
        except ValueError:
            print("Masukkan harus berupa angka. Silakan coba lagi.")
    main()
