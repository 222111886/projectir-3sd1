import argparse
import sys
from dataclasses import dataclass, asdict, field
import pandas as pd
from playwright.sync_api import sync_playwright
import time


@dataclass
class Ulasan:
    """Menyimpan informasi tentang suatu ulasan"""
    tempat: str = "Pantai Sanur"
    id_ulasan: str = None
    waktu_ulasan: str = None
    pengguna: str = None
    teks_ulasan: str = None


@dataclass
class DaftarUlasan:
    """Mengelola daftar objek Ulasan dan menyediakan metode untuk menyimpan ke Excel dan CSV"""

    daftar_ulasan: list[Ulasan] = field(default_factory=list)

    def ke_dataframe(self):
        """Mentransformasi daftar ulasan menjadi Pandas DataFrame"""
        return pd.json_normalize((asdict(ulasan) for ulasan in self.daftar_ulasan), sep="_")

    def simpan_ke_excel(self, nama_file):
        """Menyimpan ulasan ke file Excel (xlsx)"""
        self.ke_dataframe().to_excel(f"{nama_file}.xlsx", index=False)

    def simpan_ke_csv(self, nama_file):
        """Menyimpan ulasan ke file CSV"""
        self.ke_dataframe().to_csv(f"{nama_file}.csv", index=False)


def ekstrak_ulasan(halaman, total_ulasan):
    """Mengambil ulasan dari halaman lokasi Google Maps"""
    daftar_ulasan = DaftarUlasan()

    for i in range(1, total_ulasan):
        ulasan = Ulasan()
        halaman.wait_for_timeout(1000)

        # Ekstrak detail ulasan
        elemen_ulasan = halaman.query_selector(f'//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[9]/div[{3 * i - 2}]/div/div/div[2]/div[2]/div[1]/button')
        ulasan.id_ulasan = elemen_ulasan.get_attribute('data-review-id')

        waktu_ulasan_xpath = f'//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[9]/div[{3 * i - 2}]/div/div/div[4]/div[1]/span[2]'
        ulasan.waktu_ulasan = halaman.locator(waktu_ulasan_xpath).inner_text()

        nama_pengguna_xpath = f'//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[9]/div[{3 * i - 2}]/div/div/div[2]/div[2]/div[1]/button/div[1]'
        ulasan.pengguna = halaman.locator(nama_pengguna_xpath).inner_text()

        xpath_ulasan = f'//*[@id="{ulasan.id_ulasan}"]'

        if "… Lainnya" in halaman.locator(xpath_ulasan).inner_text():
            tombol_lainnya_xpath = f'//*[@id="{ulasan.id_ulasan}"]/span[2]/button'
            halaman.locator(tombol_lainnya_xpath).click()

        ulasan.teks_ulasan = halaman.locator(xpath_ulasan).inner_text()

        # Tambahkan ulasan ke daftar
        daftar_ulasan.daftar_ulasan.append(ulasan)

        # Scroll halaman
        halaman.mouse.wheel(0, 1200)
        halaman.wait_for_timeout(1000)

        # Print progress
        print(f"ID Ulasan: ...{ulasan.id_ulasan[-4:]} | Saat Ini Sudah Diambil: {i}", end='\r')
        sys.stdout.flush()

    return daftar_ulasan


def utama():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        halaman = browser.new_page()
        halaman.goto("https://maps.app.goo.gl/LkHxYWWkCsqbTZVb7", timeout=60000)

        halaman.wait_for_timeout(10000)
        halaman.locator('button:has-text("Ulasan lainnya")').click()

        print("Hak Cipta oleh Tim Scraping - 3SD1")
        print("============ Scraping ===========")

        total_ulasan = args.total + 1 if args.total else 10

        ulasan = ekstrak_ulasan(halaman, total_ulasan)

        print("\n======== Menyimpan ke Excel ========")
        ulasan.simpan_ke_excel("ulasan_pantai_sanur_gmaps")

        browser.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--total", type=int)
    args = parser.parse_args()

    utama()
